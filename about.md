# postForm
Just a small and easy task to improve your JS`s and HTML, CSS knowledge

## Task
You need to create  a mail form with next parameters:
- full name (name and family name),
- e-mail *,
- and message*

'*' means that this parameter is required and you need to check if user wrote something here.

The web page should be responsible and work on any kind of device (smartphones, tablets etc). You need to use `hover` pseudoclasses for showing where users can do some click... Create header and footer.
Check the validity of your page by next validator - [validator.w3.org](https://validator.w3.org) . Also, you should check a [CSS](http://jigsaw.w3.org/css-validator/)
**It is better to use a local library than put URL for them**

By click, your program should change the title of HTML page for showing how much emails were sent. After that, you need to show sended mail(s) on the page.

## USE ONLY:
- HTML 5,
- CSS 3,
- JavaScript (jQuery)
- Some clients or service to send mail

**Also, it is not possible to send mail by JavaScript. I would like to recommend you use [EmailJS](http://www.emailjs.com) service.**

## How does EmailJS work?
EmailJS helps sending emails using client side technologies only. No server is required – just connect EmailJS to one of the supported email services, create an email template, and use our Javascript library to trigger an email.

Email templates can optionally contain dynamic variables in almost any field (e.g. subject, content, TO address, FROM name, etc’) which are populated from the Javascript call. For example, the subject can be “{{name}}, you have a new message”, and using Javascript the name can be set to “James Dean”, for instance.

Additionally, you can easily add attachments, require CAPTCHA validation, switch between the email services without making code changes, review the history of the email request, and more.